
from dataclasses import dataclass
from datetime import datetime
from typing import Any, List
from argcomplete.compat import str


@dataclass
class DatosDeuda:
    fecha: str
    stock_deuda: int
    facturacion: int
    dso: int
    q_plan_pago: int
    q_protestos: int
    venta_relatorio: int
    costo_venta: int

@dataclass
class Promedio:
    promedio_stock_deuda: int
    promedio_facturacion: int
    promedio_dso: int
    promedio_q_plan_pago: int
    promedio_q_protestos: int
    promedio_venta_relatorio: int
    promedio_costo_venta: int



@dataclass
class DeudaVencidaMayor:
    valor: str
    porcentaje: str


@dataclass
class DatosPonderacionDso:
    dso_original: int
    riesgo_mercado: List[DeudaVencidaMayor]
    garantias_pendientes: List[DeudaVencidaMayor]
    deuda_vencida_mayor: List[DeudaVencidaMayor]
    mas_de_un_protesto: List[DeudaVencidaMayor]
    meses_informa_venta: List[DeudaVencidaMayor]



@dataclass
class GarantiasEnterada:
    soc: int
    cebe: str
    ctto: str
    tipo: str
    monto: float
    moneda: str


@dataclass
class DeudaFbl5n:
    sociedad: int
    tipo_dcto: str
    referencia: int
    fecha_pago: datetime
    mora: int
    contrato: str
    monto: int


@dataclass
class TipoVariableValor:
    clp: str
    uf: int

   
@dataclass
class VariablesDeDesicion:
    fact_prom: List[TipoVariableValor]
    deuda_actual : List[TipoVariableValor]
    deuda_prom : List[TipoVariableValor]
    garantia_ent : List[TipoVariableValor]
    exposicion : List[TipoVariableValor]
    garantia_prom : str
    cobertura_actual :str
    cobertura_prom : str
    deuda_veces_fact :str
    exposicion_porcentual : str
    
    

   
@dataclass
class GarantiasPendienteInv:
    fantasia: str
    suc: str
    ult_fecha_contrato: datetime
    gtia_solicitada: str
    gtia_entregada: str
    gtia_pendiente: str


@dataclass
class Ecp:
    rut: int
    razon_social: str
    fantasia: str
    rubro: str
    fecha_base_ecp: datetime
    tipo_cambio_uf: int
    fecha_emision_ecp: datetime
    fecha_caducidad_ecp: datetime
    dso_ponderado: int
    riesgo_determinado: str
    datos_deuda: List[DatosDeuda]
    datos_ponderacion_dso: List[DatosPonderacionDso]
    variables_de_decision: List[VariablesDeDesicion]
    garantias_enteradas: List[GarantiasEnterada]
    deudaFbl5n: List[DeudaFbl5n]
    garantias_pendiente_inv: List[GarantiasPendienteInv]

@dataclass
class response:
    codigoHttp: str
    descripcion : str
    valor : Any
     



