from dataclasses import dataclass
from typing import  List 


@dataclass
class DsoPonderado:
    valor: str

@dataclass
class TipoCliente:
    tipo_cliente: str
    dso_ponderado: str
    riesgo: str
    local: str
    modulo: str

@dataclass
class Garantia:
    resumen_matriz_garantias: List[TipoCliente]
