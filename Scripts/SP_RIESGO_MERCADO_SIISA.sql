use ecp_db;

drop procedure if exists SP_RIESGO_MERCADO_SIISA;
DELIMITER $$

CREATE procedure SP_RIESGO_MERCADO_SIISA(pRut varchar(10))

BEGIN
	SELECT DISTINCT r.riesgo, r.porcentaje 
	  FROM TBL_RIESGO_SIISA r
INNER JOIN TBL_SIISA s
		ON r.par_ordenado = s.Par_Ordenado
	 WHERE s.rut = prut
          order by s.Fecha_Actualizacion desc
     limit 1;
     
END $$

DELIMITER ;