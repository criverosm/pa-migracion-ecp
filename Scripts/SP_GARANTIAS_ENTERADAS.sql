USE ecp_db;
drop procedure IF EXISTS SP_GARANTIAS_ENTERADAS;
DELIMITER $$
create PROCEDURE SP_GARANTIAS_ENTERADAS(pRut varchar(10))
BEGIN		
		SELECT DISTINCT 
						z.sociedad  SOC,
						f.centro_beneficio CEBE,
						z.numero_contrato  CTTO,
						CASE WHEN f.indicador_cme  ='Ñ' THEN 'EFECTIVO'
								 WHEN substr(f.clase_documento,1,1) = 'G' THEN 'BOLETA' /*--valor documento primera letra G*/
								 WHEN substr(f.clase_documento,1,2) = 'KE' THEN 'DOCUMENTO' /*valor documento primeros 2*/
						END TIPO,
						 ROUND(z.cantidad,0) MONTO,
						 z.UNIDAD  UNIDAD
				FROM TBL_ZCLRE47 z
	 LEFT JOIN TBL_FBL1N f 
			  ON z.Sociedad = f.Sociedad	
         AND z.numero_Contrato  =f.Numero_contrato
			 WHERE f.cuenta=pRut
         AND (f.indicador_cme = 'Ñ' OR SUBSTR(f.clase_documento,1,1) ='G' OR SUBSTR(f.clase_documento,1,2)='KE')
			   AND (cantidad>0 AND z.unidad IS NOT NULL);
END $$
DELIMITER ;