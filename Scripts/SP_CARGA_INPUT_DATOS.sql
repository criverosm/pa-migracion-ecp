USE ecp_db;
DROP PROCEDURE IF EXISTS SP_CARGA_INPUT_DATOS;
DELIMITER $$
CREATE PROCEDURE SP_CARGA_INPUT_DATOS(pTabla varchar(50))
 	
BEGIN
  declare min_fecha datetime;
  declare max_fecha datetime;
IF pTabla = 'TBL_PRECIO_UF' OR pTabla = 'ALL' THEN
	   INSERT INTO TBL_PRECIO_UF
		SELECT distinct 
				s.Ano,
				s.mes,
				s.dia,
				CAST(s.fecha AS DATETIME),
				REPLACE(s.Precio,',','.') ,
				s.unidad
			FROM STG_TBL_PRECIO_UF s
	   LEFT JOIN TBL_PRECIO_UF T
			  ON s.FECHA = T.FECHA
			WHERE T.FECHA IS NULL;
								 				
END IF;
IF pTabla = 'TBL_BUC_BERTIN'  OR pTabla = 'ALL' THEN
    
	TRUNCATE TABLE TBL_BUC_BERTIN;
	   INSERT INTO TBL_BUC_BERTIN
			 SELECT s.Rut_Cuenta_Deudor	,s.DV,s.Q,s.DVR,s.Razon_Social,s.Fantasia,s.Holding_Grupo,s.SubGte_COM,s.Rubro_CC,
					s.Rubro_CC_Especifico,s.Ejecutivo_CC,s.NdC,s.Enviar_Fisico_IBR,s.Tipo_Cliente_IBR,s.Contacto_Receptor_Factura_PDF_Paperless_DEFINITIVO,
					s.Propuesta_contacto_receptor_factura_pdf_paperless,s.Propuesta_correo_contacto_IBR,s.Propuesta_telefono_Contacto_IBR,s.Forma_de_Pago_IBR,
					s.Modalidad_de_Pago_IBR,s.Nombre_Completo_1,s.Cargo_1,s.Especialidad_Rubro_que_Paga,s.SUC_CTTO,s.Email_1,s.Fono_1,s.Nombre_Completo2,
					s.Cargo_2,s.Email_2,s.Fono_2,s.Direccion_de_despacho,s.Nombre_3,s.Email_3,s.Fono_3,s.Fono_4,s.Email_4,s.Email_5,s.Email_6,
					s.Email_7, cast(s.Fecha_Actualizacion as datetime)
			  FROM STG_TBL_BUC_BERTIN s;
  END IF;                    
IF pTabla = 'TBL_FBL1N'  OR pTabla = 'ALL' THEN 
			TRUNCATE TABLE TBL_FBL1N;
			   INSERT INTO TBL_FBL1N
					SELECT S.Sociedad,
							S.Cuenta,
							S.Numero_contrato,
							S.Centro_beneficio,
							S.N_documento,
							S.Asignacion,
							S.Indicador_CME,
							S.Clase_documento,
							S.Referencia,
							CAST(S.Fecha_entrada as datetime),
							CAST(S.Fecha_documento as datetime),
							S.Condiciones_pago,
							CAST(S.Fecha_pago as datetime),
							S.Demora_en_DPP1,
							S.Moneda_local,
							REPLACE(S.Importe_moneda_local,',','.') ,
							S.Moneda_documento,
							REPLACE(S.Importe_moneda_doc,',','.') ,
							S.Clave_referencia1,
							S.Texto,
							S.Ejercicio,
							cast(S.Fecha_Actualizacion as datetime)
							FROM STG_TBL_FBL1N S;			
END IF;
IF pTabla = 'TBL_FBL5N'  OR pTabla = 'ALL' THEN 

	
                      
		 delete TBL_FBL5N
     from TBL_FBL5N
         where Fe_contabilizacion >= ( select min(Fe_contabilizacion)
									     from STG_TBL_FBL5N
									    where Fe_contabilizacion is not null)
           and Fe_contabilizacion is not null;
         
	  INSERT INTO TBL_FBL5N (Icono_partabiertas_comp,Sociedad,Segmento,Centro_de_beneficio,Cuenta,Cuenta_de_mayor,Indicador_CME,
      Clase_de_documento,Referencia,Asignacion,Clave_referencia1,Fe_contabilizacion,Fecha_de_pago,Demora_tras_vencimiento_neto,
      Importe_en_moneda_local,Numero_de_contrato,Texto,Fecha_compensacion,Doc_compensacion,id)
		   	SELECT
					S.Icono_partabiertas_comp,
					S.Sociedad,
					S.Segmento,
					S.Centro_de_beneficio,
					S.Cuenta,
					S.Cuenta_de_mayor,
					S.Indicador_CME,
					S.Clase_de_documento,
					S.Referencia,
					S.Asignacion,
					S.Clave_referencia1,
				    Fe_contabilizacion,
					CAST(S.Fecha_de_pago AS DATETIME),
					REPLACE(S.Demora_tras_vencimiento_neto,',','.'),
					REPLACE(S.Importe_en_moneda_local,',','.'),
					S.Numero_de_contrato,
					S.Texto,
					CAST(S.Fecha_compensacion AS DATETIME),
					S.Doc_compensacion,
					S.id
			FROM STG_TBL_FBL5N S;
            
END IF;
IF pTabla = 'TBL_SIISA'  OR pTabla = 'ALL' THEN 

TRUNCATE TABLE TBL_SIISA;
     INSERT INTO TBL_SIISA
		   SELECT	s.RUT,	
					s.DV,
					s.NOMBRE,
					s.Par_Ordenado,
					replace(s.HT_CH,',','.'),
					replace(s.P_CH,',','.')	,
					replace(s.HT_LT,',','.'),
					replace(s.P_LT,',','.')	,
					replace(s.HT_PG,',','.'),
					replace(s.P_PG,',','.')	,
					replace(s.HT_CM,',','.'),
					replace(s.P_CM,',','.')	,
					replace(s.HT_MIC,',','.'),
					replace(s.P_MIC,',','.'),
					replace(s.HT_IF,',','.'),
					replace(s.P_IF,',','.')	,
					replace(s.HT_M,',','.')	,
					replace(s.P_M,',','.')	,
					s.RANKING ,
					REPLACE(s.GENERAL,',','.')	, 
					cast(s.Fecha_Actualizacion 	 as DATETIME)
		 FROM STG_TBL_SIISA s;
END IF;
IF pTabla = 'TBL_ZCLRE46'  OR pTabla = 'ALL' THEN   
      
 TRUNCATE TABLE TBL_ZCLRE46;
	INSERT INTO TBL_ZCLRE46
		 SELECT S.SOCIEDAD,
				S.UE,
				S.Nro_Contrato,
				S.Fantasia,
				S.SUC,
				S.Denominacion_gpo_autorizacion ,
				S.Denominacion 		,
				CAST(S.Fecha_Inicio 	AS DATETIME),
				CAST(S.Fecha_Termino 	AS DATETIME),
				CAST(S.InicContr 		AS DATETIME),
				CAST(S.Fin_Contrato 	AS DATETIME),
				S.Rescis_Contrato 	,
				S.ReVN L,
				S.Interl_comercial	,
				S.Nombre_Completo  ,
				CAST(S.Inic_rel AS DATETIME),
				CAST(S.Final_rel AS DATETIME),
				replace(S.Base,',','.'),
				S.Un,
				replace(S.Base1,',','.'),
				S.Un_1 		,
				S.ST_CTR ,
				S.ST_OBJ
    FROM STG_TBL_ZCLRE46 S;
END IF;
IF pTabla = 'TBL_ZCLRE47'  OR pTabla = 'ALL' THEN   

		
TRUNCATE TABLE TBL_ZCLRE47;
		   INSERT INTO TBL_ZCLRE47
			    SELECT  s.Sociedad,
						s.numero_contrato ,
						s.Denominacion_contrato,
                          CAST(CONCAT(SUBSTRING(s.InicContr,7,4),'-' , SUBSTRING(s.InicContr,4,2) , '-' , SUBSTRING(s.InicContr,1,2)) AS DATETIME),
                        CAST(CONCAT(SUBSTRING(s.Fin_vigen,7,4),'-' , SUBSTRING(s.Fin_vigen,4,2) , '-' , SUBSTRING(s.Fin_vigen,1,2)) AS DATETIME),
                        CAST(CONCAT(SUBSTRING(s.PrimFinCon,7,4),'-' , SUBSTRING(s.PrimFinCon,4,2) , '-' , SUBSTRING(s.PrimFinCon,1,2)) AS DATETIME),
						CAST(CONCAT(SUBSTRING(Rescis_el,7,4),'-' , SUBSTRING(Rescis_el,4,2) , '-' , SUBSTRING(Rescis_el,1,2)) AS DATETIME),
						Interl_comercial,
                        Final_rel,
                        TpMed,
                        Tipo_medida,
                        CAST(CONCAT(SUBSTRING(s.Medida_de,7,4),'-' , SUBSTRING(s.Medida_de,4,2) , '-' , SUBSTRING(s.Medida_de,1,2)) AS DATETIME),
                        CAST(CONCAT(SUBSTRING(s.Medida_a,7,4),'-' , SUBSTRING(s.Medida_a,4,2) , '-' , SUBSTRING(s.Medida_a,1,2)) AS DATETIME),
                        REPLACE(s.Cantidad,',','.'),
						s.UNIDAD,
						s.STATUS,
						s.vigente
			      FROM STG_TBL_ZCLRE47 s;
 
END IF;

IF pTabla = 'TBL_ZCLRE53'  OR pTabla = 'ALL' THEN   

 TRUNCATE TABLE TBL_ZCLRE53;
   INSERT INTO TBL_ZCLRE53
         SELECT cliente,
				rut,
				nombre	,
				direccion,
				distrito,
				poblacion,
				giro_comercial,
				telefono,
				fax,
				email	,
				CAST(f_creacion AS DATETIME),
				CAST(fecha_archivo AS DATETIME)
		 FROM STG_TBL_ZCLRE53;
END IF;

IF pTabla = 'TBL_RELATORIOS_BW'  OR pTabla = 'ALL' THEN   

			TRUNCATE TABLE TBL_RELATORIOS_BW;
			   INSERT INTO TBL_RELATORIOS_BW
					SELECT cliente,
							nombre,
							Ano_mes_natural as Ano_mes_original,
							CASE WHEN COALESCE(Ano_mes_natural,0) = 0 THEN NULL 
								 ELSE LAST_DAY(CONCAT(SUBSTRING(Ano_mes_natural,4,4) ,'-' ,SUBSTRING(Ano_mes_natural,1,2) , '-01'))
							END,
							REPLACE(Venta,',','.') venta
					   FROM STG_TBL_RELATORIOS_BW;
END IF; 
 IF pTabla = 'TBL_BW' OR pTabla = 'ALL'  THEN   
			TRUNCATE TABLE TBL_BW;
			   INSERT INTO TBL_BW
					SELECT sociedad,
							clave_referencia1,
							cliente,
							razon_social_tmp,
						    CASE WHEN COALESCE(vencimiento_neto,0) =0 THEN NULL
							     ELSE CONCAT(SUBSTRING(vencimiento_neto,7,4),'-' , SUBSTRING(vencimiento_neto,4,2) , '-' , SUBSTRING(vencimiento_neto,1,2))
						    END,
						    contrato,
							nombre_fantasia_tmp,
							indicador_cme,
							Descripcion_Indicador_CME_tmp,
							clase_documento,
							descripcion_clase_documento_tmp,
							REPLACE(REPLACE(importe,'.',''),',','.') 
					   FROM STG_TBL_BW;

	END IF;
    
     IF pTabla = 'TBL_REISCN' OR pTabla = 'ALL'  THEN  
			
	INSERT INTO TBL_REISCN
		SELECT Sociedad,
				Clase_contrato,
				Contrato numero_contrato,
				Den_cl_contrato,
				denominacion_contrato,
				 CAST(CONCAT(SUBSTRING(s.linea_valida_de,7,4),'-' , SUBSTRING(s.linea_valida_de,4,2) , '-' , SUBSTRING(s.linea_valida_de,1,2)) AS DATETIME),
				 CAST(CONCAT(SUBSTRING(s.primer_fin_contrato,7,4),'-' , SUBSTRING(s.primer_fin_contrato,4,2) , '-' , SUBSTRING(s.primer_fin_contrato,1,2)) AS DATETIME),
				 cAST(CONCAT(SUBSTRING(s.contabil_desde,7,4),'-' , SUBSTRING(s.contabil_desde,4,2) , '-' , SUBSTRING(s.contabil_desde,1,2)) AS DATETIME),
				 cAST(CONCAT(SUBSTRING(s.Fe_in_flj_financiero,7,4),'-' , SUBSTRING(s.Fe_in_flj_financiero,4,2) , '-' , SUBSTRING(s.Fe_in_flj_financiero,1,2)) AS DATETIME),
				 UnidadEconomicaContr,
				 Denominacion_gpo_autorizacion,
				 Sector_industrial,
				 Denominacion_ramo,
				 Inventario_economico,
				 Status_actual,
				 Status_actual2,
				 Rescision_el,
				 Per_vigencia_meses,
				 cAST(CONCAT(SUBSTRING(s.Linea_valida_hasta,7,4),'-' , SUBSTRING(s.Linea_valida_hasta,4,2) , '-' , SUBSTRING(s.Linea_valida_hasta,1,2)) AS DATETIME),
				 autor
			FROM STG_TBL_REISCN s;
     END IF;
	 
	 
	IF pTabla = 'TBL_RIESGO_SIISA' OR pTabla = 'ALL'  THEN  
		
		TRUNCATE TABLE TBL_RIESGO_SIISA;
		INSERT INTO TBL_RIESGO_SIISA VALUES ('A1','RN1',-0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('A2','RN2',-0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('A3','RN3',-0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('A4','RN4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('A5','RN5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('A6','RN6',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('A7','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('A8','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('A9','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('B1','RN2',-0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('B2','RN3',-0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('B3','RN4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('B4','RN5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('B5','RN6',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('B6','RS1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('B7','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('B8','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('B9','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('C1','RN3',-0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('C2','RN4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('C3','RN5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('C4','RN6',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('C5','RS1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('C6','RS2',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('C7','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('C8','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('C9','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('D1','RN4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('D2','RN5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('D3','RN6',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('D4','RS1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('D5','RS2',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('D6','RS3',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('D7','RS4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('D8','RS5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('D9','AR1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('E1','RN5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('E2','RN6',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('E3','RS1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('E4','RS2',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('E5','RS3',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('E6','RS4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('E7','RS5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('E8','AR1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('E9','AR2',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('F1','RN6',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('F2','RS1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('F3','RS2',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('F4','RS3',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('F5','RS4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('F6','RS5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('F7','AR1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('F8','AR2',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('F9','AR3',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('G1','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('G2','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('G3','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('G4','RS4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('G5','RS5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('G6','AR1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('G7','AR2',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('G8','AR3',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('G9','AR4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('H1','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('H2','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('H3','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('H4','RS5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('H5','AR1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('H6','AR2',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('H7','AR3',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('H8','AR4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('H9','AR5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('I1','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('I2','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('I3','RI',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('I4','AR1',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('I5','AR2',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('I6','AR3',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('I7','AR4',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('I8','AR5',0.06);
		INSERT INTO TBL_RIESGO_SIISA VALUES ('I9','AR6',0.06);
	 END IF;
	 
	 IF pTabla = 'TBL_TABLA_CONVERSION' OR pTabla = 'ALL'  THEN  
		
		TRUNCATE TABLE TBL_TABLA_CONVERSION; 
	 
	 INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','clase_documento','DA');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','clase_documento','DE');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','clase_documento','DR');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','clase_documento','L2');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','clase_documento','NA');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','clase_documento','ND');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','clase_documento','NE');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','clase_documento','Z1');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','clase_documento','Z2');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','indicador_cme','H');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','indicador_cme','I');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','indicador_cme','A');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','indicador_cme',NULL);
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','indicador_cme','');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','cuenta_de_mayor','111401001');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','cuenta_de_mayor','111401003');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','cuenta_de_mayor','111402001');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','cuenta_de_mayor','111402003');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','cuenta_de_mayor','111401005');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','cuenta_de_mayor','111402005');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','facturacion','cuenta_de_mayor','111401007');

		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','stock_deuda','cuenta_de_mayor','111401001');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','stock_deuda','cuenta_de_mayor','111401003');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','stock_deuda','cuenta_de_mayor','111402001');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','stock_deuda','cuenta_de_mayor','111402003');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','stock_deuda','cuenta_de_mayor','111401005');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','stock_deuda','cuenta_de_mayor','111402005');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','stock_deuda','cuenta_de_mayor','111401007');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','q_plan_pago','clase_documento','ZS');

		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','q_protestos','clase_documento','ZS');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','q_protestos','clase_documento','DZ');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','q_protestos','clase_documento','CH');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','q_protestos','clase_documento','CI');
		INSERT INTO TBL_TABLA_CONVERSION VALUES('20191226','q_protestos','indicador_cme','I');
	 
	 END IF;
	 
	 
	 IF pTabla = 'TBL_MATRIZ_GARANTIAS' OR pTabla = 'ALL'  THEN  
		
		TRUNCATE TABLE TBL_MATRIZ_GARANTIAS; 
	 
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(1,'TRANSVERSAL','Entre 0 y 29','BAJO','3 GAT','3 GAT');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(2,'TRANSVERSAL','Entre 30 y 39','MEDIO','5 GAT','4 GAT');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(3,'TRANSVERSAL','Entre 40 y 49','ALTO','7 GAT','5 GAT');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(4,'TRANSVERSAL','Igual o mayor a 50','RECHAZADO','7 GAT y CODEUDOR','5 GAT y CODEUDOR');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(5,'OUTLET y STRIP CENTER','Entre 0 y 29','BAJO','3 GAT','3 GAT');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(6,'OUTLET y STRIP CENTER','Entre 30 y 39','MEDIO','4 GAT','4 GAT');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(7,'OUTLET y STRIP CENTER','Entre 40 y 49','ALTO','5 GAT','5 GAT');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(8,'OUTLET y STRIP CENTER','Igual o mayor a 50','RECHAZADO','5 GAT y CODEUDOR','5 GAT y CODEUDOR');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(9,'EXCLUSIVOS PAE / ASA / ACO','Entre 0 y 39','BAJO','2 GAT','3 GAT');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(10,'EXCLUSIVOS PAE / ASA / ACO','Entre 40 y 49','MEDIO','3 GAT','4 GAT');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(11,'EXCLUSIVOS PAE / ASA / ACO','Entre 50 y 59','ALTO','4 GAT','5 GAT');
			INSERT INTO TBL_MATRIZ_GARANTIAS VALUES(12,'EXCLUSIVOS PAE / ASA / ACO','Igual o mayor a 60','RECHAZADO','4 GAT y CODEUDOR','5 GAT y CODEUDOR');

	 
	 END IF;
	 
	  
	 IF pTabla = 'TBL_LISTAS' OR pTabla = 'ALL'  THEN  
		
		TRUNCATE TABLE TBL_LISTAS; 

				INSERT INTO TBL_LISTAS VALUES('DR','','DR','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('DA','','DA','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('DE','','DE','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('NA','','NA','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('NE','','NE','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('NC','','NC','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('L1','','L1','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('L2','','L2','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('Z1','','Z1','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('Z2','','Z2','Facturacion');
				INSERT INTO TBL_LISTAS VALUES('HZS','H','ZS','Ch. Fecha');
				INSERT INTO TBL_LISTAS VALUES('HDZ','H','DZ','Ch. Fecha');
				INSERT INTO TBL_LISTAS VALUES('HCH','H','CH','Ch. Fecha');
				INSERT INTO TBL_LISTAS VALUES('HCI','H','CI','Ch. Fecha');
				INSERT INTO TBL_LISTAS VALUES('IZS','I','ZS','Protesto');
				INSERT INTO TBL_LISTAS VALUES('IDZ','I','DZ','Protesto');
				INSERT INTO TBL_LISTAS VALUES('ICH','I','CH','Protesto');
				INSERT INTO TBL_LISTAS VALUES('ICI','I','CI','Protesto');
				INSERT INTO TBL_LISTAS VALUES('DK','','DK','Saldos');
				INSERT INTO TBL_LISTAS VALUES('DD','','DD','Abono');
				INSERT INTO TBL_LISTAS VALUES('ND','','ND','Nota Debito');

	 END IF;
	 
	 
END $$

DELIMITER ;