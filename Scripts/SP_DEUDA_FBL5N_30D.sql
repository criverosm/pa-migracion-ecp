DROP PROCEDURE if exists SP_DEUDA_FBL5N_30D;

DELIMITER $$
CREATE PROCEDURE SP_DEUDA_FBL5N_30D(pRut varchar(10))
  
BEGIN
					SELECT distinct b.SOCIEDAD,coalesce(l.NOMBRE,b.Clase_documento) NOMBRE,
b.CLAVE_REFERENCIA1, b.VENCIMIENTO_NETO , 
                            ABS( datediff(cast(substr(cast(now() as datetime),1,10) as datetime),cast(b.VENCIMIENTO_NETO as datetime))) MORA, 
                            b.CONTRATO, 
                            b.IMPORTE MONTO
					 FROM TBL_BW b
		       LEFT JOIN TBL_LISTAS l
						ON l.indicador_cme   = b.indicador_cme
					  AND l.Clase_documento = b.Clase_documento
                      AND l.NOMBRE not like '%Ch. Fecha%'
					WHERE b.cliente = pRut /*9113863'*/
					 AND  datediff(cast(b.VENCIMIENTO_NETO as datetime),cast(substr(cast(now() as datetime),1,10) as datetime))  <-30
                     and b.importe>0
                     order by b.IMPORTE desc, b.VENCIMIENTO_NETO asc;
END $$

DELIMITER ;
