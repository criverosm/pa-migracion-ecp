use ecp_db;
DROP PROCEDURE IF EXISTS  SP_LIMPIA_DATOS_STG;
DELIMITER $$
create procedure SP_LIMPIA_DATOS_STG(pTabla VARCHAR(50))
BEGIN
		
 IF pTabla = 'TBL_BUC_BERTIN' OR pTabla = 'ALL'  THEN   
			DELETE FROM STG_TBL_BUC_BERTIN
				  WHERE Rut_Cuenta_Deudor IS NULL;

			DELETE FROM STG_TBL_BUC_BERTIN
			       WHERE rut_Cuenta_deudor ='NULL';

			UPDATE STG_TBL_BUC_BERTIN
			SET DV = NULL
			WHERE DV='NULL';
END IF;

 IF pTabla = 'TBL_FBL1N' OR pTabla = 'ALL'  THEN   
 
			UPDATE STG_TBL_FBL1N SET FECHA_PAGO       	 = NULL	WHERE FECHA_PAGO         = 'NULL';
			UPDATE STG_TBL_FBL1N SET Fecha_entrada     	 = NULL WHERE Fecha_entrada      ='NULL';
			UPDATE STG_TBL_FBL1N SET Fecha_documento  	 = NULL WHERE Fecha_documento    ='NULL';
			UPDATE STG_TBL_FBL1N SET Fecha_pago 		 = NULL WHERE Fecha_pago         ='NULL';
			UPDATE STG_TBL_FBL1N SET Fecha_Actualizacion = NULL WHERE Fecha_Actualizacion='NULL';
			UPDATE STG_TBL_FBL1N SET indicador_cme       = 'Ñ'  WHERE indicador_cme='Ã‘';

END IF;

 IF pTabla = 'TBL_FBL5N' OR pTabla = 'ALL'  THEN   
 
 			UPDATE STG_TBL_FBL5N 
			   SET Fe_contabilizacion = NULL
			 WHERE Fe_contabilizacion = 'NULL' OR Fe_contabilizacion= '0000-00-00 00:00:00' OR Fe_contabilizacion ='';
           
			            
			UPDATE STG_TBL_FBL5N
			   SET Demora_tras_vencimiento_neto = NULL
			 WHERE Demora_tras_vencimiento_neto = 'NULL';

			UPDATE STG_TBL_FBL5N 
			   SET Fecha_compensacion = NULL
			 WHERE Fecha_compensacion = 'NULL' OR Fecha_compensacion = '';
           
			UPDATE STG_TBL_FBL5N 
			   SET Fecha_de_pago = NULL
			 WHERE Fecha_de_pago = 'NULL' OR Fecha_de_pago = '';

			UPDATE STG_TBL_FBL5N 
			   SET sociedad = NULL
			 WHERE sociedad = '';

			UPDATE STG_TBL_FBL5N 
			   SET Cuenta_de_mayor = NULL
			 WHERE Cuenta_de_mayor = '';

			UPDATE STG_TBL_FBL5N 
			   SET Doc_compensacion = NULL
			 WHERE Doc_compensacion = '';
			
  /*       
            DELETE STG_TBL_FBL5N 
			  FROM STG_TBL_FBL5N 
			 WHERE fe_contabilizacion IS NULL;
*/
		/* fecha = dd-mm-yyyy */
                update STG_TBL_FBL5N
                 set Fe_contabilizacion = STR_TO_DATE(Fe_contabilizacion,'%d-%m-%Y')
              where Fe_contabilizacion like '%-%' and  Fe_contabilizacion not like '%00:00:00%';
              
				/*fecha dd/mm/yyyy */
				update STG_TBL_FBL5N
              set Fe_contabilizacion = STR_TO_DATE(Fe_contabilizacion,'%d/%m/%Y')
              where Fe_contabilizacion like '%/%';
              
		
			/* fecha se deja en formato YYYYMMDD */
            update STG_TBL_FBL5N
              set Fe_contabilizacion = replace(substr(Fe_contabilizacion,1,10),'-','')
              where Fe_contabilizacion like '%-%';
              
              
				update STG_TBL_FBL5N
                 set Fecha_de_pago = STR_TO_DATE(Fecha_de_pago,'%d-%m-%Y')
              where Fecha_de_pago like '%-%' and  Fecha_de_pago not like '%00:00:00%';
              
              
              
			 update STG_TBL_FBL5N
              set Fecha_de_pago = STR_TO_DATE(Fecha_de_pago,'%d/%m/%Y')
              where Fecha_de_pago like '%/%';
              
			update STG_TBL_FBL5N
              set Fecha_de_pago = replace(substr(Fecha_de_pago,1,10),'-','')
              where Fecha_de_pago like '%-%';
              
              
				update STG_TBL_FBL5N
                 set Fecha_compensacion = STR_TO_DATE(Fecha_compensacion,'%d-%m-%Y')
              where Fecha_compensacion like '%-%' and  Fecha_compensacion not like '%00:00:00%';
              
              
			 update STG_TBL_FBL5N
              set Fecha_compensacion = STR_TO_DATE(Fecha_compensacion,'%d/%m/%Y')
              where Fecha_compensacion like '%/%';
              
                
			update STG_TBL_FBL5N
              set Fecha_compensacion = replace(substr(Fecha_compensacion,1,10),'-','')
              where Fecha_compensacion like '%-%';
              
			    /*update STG_TBL_FBL5N
                    set Fe_contabilizacion = STR_TO_DATE(Fe_contabilizacion,'%d/%m/%Y')
                    where Fe_contabilizacion like '%/%'
                       and Fe_contabilizacion not like '%-%'
                      and Fe_contabilizacion not like '%00:00:00%';
                    */
				

END IF;

 IF pTabla = 'TBL_SIISA' OR pTabla = 'ALL'  THEN  
			UPDATE STG_TBL_SIISA
			SET DV = NULL
			WHERE DV = 'NULL';
END IF;

 IF pTabla = 'TBL_ZCLRE46' OR pTabla = 'ALL'  THEN  
			update STG_TBL_ZCLRE46 set Rescis_Contrato = null where Rescis_Contrato  = 'NULL';
			update STG_TBL_ZCLRE46 set Inic_rel  = null where Inic_rel  = 'NULL';
			update STG_TBL_ZCLRE46 Set Fecha_Inicio = NULL where Fecha_Inicio = 'NULL';
			update STG_TBL_ZCLRE46 Set Fecha_Termino = NULL  where Fecha_Termino = 'NULL';
			update STG_TBL_ZCLRE46 Set InicContr = NULL  where InicContr = 'NULL';
			update STG_TBL_ZCLRE46 Set Fin_Contrato = NULL  where Fin_Contrato = 'NULL';
			update STG_TBL_ZCLRE46 Set Fecha_Inicio = NULL  where Fecha_Inicio = 'NULL';
			update STG_TBL_ZCLRE46 Set Final_rel = NULL  	 where Final_rel	 = 'NULL';

END IF;

 IF pTabla = 'TBL_ZCLRE47' OR pTabla = 'ALL'  THEN  
			UPDATE STG_TBL_ZCLRE47 SET Interl_comercial 	= NULL WHERE Interl_comercial = 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET sociedad				= NULL WHERE sociedad = 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET NUMERO_CONTRATO 		= NULL WHERE NUMERO_CONTRATO = 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET DENOMINACION_CONTRATO = NULL WHERE DENOMINACION_CONTRATO = 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET InicContr 			= NULL WHERE InicContr= 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET Fin_vigen 			= NULL WHERE Fin_vigen= 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET Cantidad 			= NULL WHERE Cantidad = 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET UNIDAD 				= NULL WHERE UNIDAD = 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET Medida_de 			= NULL WHERE Medida_de= 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET Medida_a 			= NULL WHERE Medida_a= 'NULL';
			UPDATE STG_TBL_ZCLRE47 SET STATUS 				= NULL WHERE STATUS = 'NULL';

			
			
                  
			delete from STG_TBL_ZCLRE47
			where sociedad =''
			and numero_contrato='';

			UPDATE STG_TBL_ZCLRE47 SET  Medida_de = NULL WHERE Medida_de ='';
			UPDATE STG_TBL_ZCLRE47 SET  Medida_a = NULL WHERE Medida_a ='';
			UPDATE STG_TBL_ZCLRE47 SET  InicContr = NULL WHERE InicContr ='';
			UPDATE STG_TBL_ZCLRE47 SET  Fin_vigen = NULL WHERE Fin_vigen ='';
			UPDATE STG_TBL_ZCLRE47 SET  PrimFinCon = NULL WHERE PrimFinCon ='';
			UPDATE STG_TBL_ZCLRE47 SET  Rescis_el = NULL WHERE Rescis_el ='';

END IF;

 IF pTabla = 'TBL_ZCLRE53' OR pTabla = 'ALL'  THEN  
			UPDATE STG_TBL_ZCLRE53 SET cliente		 = NULL WHERE cliente = 'NULL';
			UPDATE STG_TBL_ZCLRE53 SET f_creacion 	 = NULL WHERE f_creacion = 'NULL';
			UPDATE STG_TBL_ZCLRE53 SET fecha_archivo = NULL WHERE fecha_archivo = 'NULL';

END IF;

 IF pTabla = 'TBL_RELATORIOS_BW' OR pTabla = 'ALL'  THEN  

			UPDATE STG_TBL_RELATORIOS_BW
			   SET ano_mes_original = NULL
			 WHERE ano_mes_original = '';
END IF;

 IF pTabla = 'TBL_BW' OR pTabla = 'ALL'  THEN  
			DELETE FROM STG_TBL_BW
			      WHERE sociedad= 'Resultado total';

			UPDATE STG_TBL_BW
			   SET vencimiento_neto = NULL
			WHERE vencimiento_neto LIKE '%#%';

			 UPDATE STG_TBL_BW
			 SET vencimiento_neto = NULL
			 WHERE vencimiento_neto = '';

			 UPDATE STG_TBL_BW
			  set indicador_cme=replace(replace(indicador_cme,'D/',''),'#','');
			  
	END IF;


 
 END $$

DELIMITER ;